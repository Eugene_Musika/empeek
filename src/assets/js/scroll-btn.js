$(document).ready(function(){
	var $scroll = $('.js-scroll-to'),
	    isMobile;

	//Hiding the button first
	$('.js-scroll-to').hide();

	$(window).scroll(function() {
		isMobile = /mobil/i.test(navigator.userAgent);

		// 300 = The point you would like to fade the scroll button.
		if (($(window).scrollTop() > 300) && !isMobile){

		//ISSUE: don't faiding slow, it happing instantly WHY?
			$scroll.fadeIn('slow ease-in');

		} else {

			$scroll.fadeOut('slow ease-in');
		};
	});

	$scroll.click(function(e){
		e.preventDefault();

		$('html, body').animate({
			scrollTop: $(this.hash).offset().top
		}, 1500);
	});

});
