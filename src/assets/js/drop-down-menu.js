(function () {
	$('.js-menu-mobile-button').on('change', function() {
		$('.js-main-nav-list, \
			 .js-header__content, \
			 .js-main-nav__mobile-blind' ).toggleClass('is-active');
	})
})();
